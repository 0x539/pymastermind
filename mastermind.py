"""

this is the code of the Mastermind object

Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

from random import randrange

class Mastermind:

	def __init__(self):
		#La liste des couleurs de l'objet
		self.couleurs = [0,0,0,0]
		self.gagne = False
		self.RemplirCouleurs()
		#Nombre de couleurs Bien Places
		self.nbBP = 0
		#Nombre de couleurs Mal Places
		self.nbMP = 0

	def RemplirCouleurs(self):
		i = 0
		while i < 4:
			existe = True
			while existe==True:
				existe = False
				randNb = randrange(1,9)

				for nb in self.couleurs:
					if randNb == nb:
						existe = True

				if existe == False:
					self.couleurs[i] = randNb		
				
			i += 1		

	def tricher(self):
		print(self.couleurs)

	def verifier(self, couleur1, couleur2, couleur3, couleur4):
		listeAVerif = [couleur1, couleur2, couleur3, couleur4]

		compteur = 0
		compteur2 = 0
		self.nbBP = 0
		self.nbMP = 0
		while compteur < 4:
			while compteur2 < 4:
				if listeAVerif[compteur] == self.couleurs[compteur2]:
					self.nbMP += 1
				compteur2 += 1
			if self.couleurs[compteur] == listeAVerif[compteur]:
				self.nbBP += 1

			compteur += 1
			compteur2 = 0

		self.nbMP -= self.nbBP
		if self.nbBP == 4:
			self.gagne = True
