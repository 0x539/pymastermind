"""

this is the code of the GUI

Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_GroupBox(object):
    def setupUi(self, GroupBox):
        GroupBox.setObjectName(_fromUtf8("GroupBox"))
        GroupBox.resize(400, 300)
        GroupBox.setTitle(_fromUtf8(""))
        self.gridLayoutWidget = QtGui.QWidget(GroupBox)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(2, 0, 401, 301))
        self.gridLayoutWidget.setObjectName(_fromUtf8("gridLayoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label = QtGui.QLabel(self.gridLayoutWidget)
        self.label.setLineWidth(1)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 5, 0, 1, 1)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 9, 0, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 4, 0, 1, 1)
        self.btnNP = QtGui.QPushButton(self.gridLayoutWidget)
        self.btnNP.setObjectName(_fromUtf8("btnNP"))
        self.gridLayout.addWidget(self.btnNP, 8, 2, 1, 1)
        self.btnEssayer = QtGui.QPushButton(self.gridLayoutWidget)
        self.btnEssayer.setObjectName(_fromUtf8("btnEssayer"))
        self.gridLayout.addWidget(self.btnEssayer, 8, 0, 1, 1)
        self.couleur1 = QtGui.QComboBox(self.gridLayoutWidget)
        self.couleur1.setObjectName(_fromUtf8("couleur1"))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.couleur1.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.couleur1, 1, 0, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem2, 2, 0, 1, 1)
        self.couleur2 = QtGui.QComboBox(self.gridLayoutWidget)
        self.couleur2.setObjectName(_fromUtf8("couleur2"))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.couleur2.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.couleur2, 1, 2, 1, 1)
        self.label_2 = QtGui.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 6, 0, 1, 1)
        self.couleur4 = QtGui.QComboBox(self.gridLayoutWidget)
        self.couleur4.setObjectName(_fromUtf8("couleur4"))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.couleur4.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.couleur4, 3, 0, 1, 1)
        self.couleur3 = QtGui.QComboBox(self.gridLayoutWidget)
        self.couleur3.setObjectName(_fromUtf8("couleur3"))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.couleur3.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.couleur3, 3, 2, 1, 1)
        self.nbBP = QtGui.QLabel(self.gridLayoutWidget)
        self.nbBP.setObjectName(_fromUtf8("nbBP"))
        self.gridLayout.addWidget(self.nbBP, 5, 2, 1, 1)
        self.nbMP = QtGui.QLabel(self.gridLayoutWidget)
        self.nbMP.setObjectName(_fromUtf8("nbMP"))
        self.gridLayout.addWidget(self.nbMP, 6, 2, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem3, 0, 0, 1, 1)

        self.retranslateUi(GroupBox)
        QtCore.QMetaObject.connectSlotsByName(GroupBox)

    def retranslateUi(self, GroupBox):
        GroupBox.setWindowTitle(_translate("GroupBox", "pymastermind", None))
        self.label.setText(_translate("GroupBox", "Couleurs Bien placés :", None))
        self.btnNP.setText(_translate("GroupBox", "Nouvelle Partie", None))
        self.btnEssayer.setText(_translate("GroupBox", "Essayer", None))
        self.couleur1.setItemText(0, _translate("GroupBox", "Rouge", None))
        self.couleur1.setItemText(1, _translate("GroupBox", "Jaune", None))
        self.couleur1.setItemText(2, _translate("GroupBox", "Vert", None))
        self.couleur1.setItemText(3, _translate("GroupBox", "Bleu", None))
        self.couleur1.setItemText(4, _translate("GroupBox", "Orange", None))
        self.couleur1.setItemText(5, _translate("GroupBox", "Blanc", None))
        self.couleur1.setItemText(6, _translate("GroupBox", "Violet", None))
        self.couleur1.setItemText(7, _translate("GroupBox", "Fuschia", None))
        self.couleur2.setItemText(0, _translate("GroupBox", "Rouge", None))
        self.couleur2.setItemText(1, _translate("GroupBox", "Jaune", None))
        self.couleur2.setItemText(2, _translate("GroupBox", "Vert", None))
        self.couleur2.setItemText(3, _translate("GroupBox", "Bleu", None))
        self.couleur2.setItemText(4, _translate("GroupBox", "Orange", None))
        self.couleur2.setItemText(5, _translate("GroupBox", "Blanc", None))
        self.couleur2.setItemText(6, _translate("GroupBox", "Violet", None))
        self.couleur2.setItemText(7, _translate("GroupBox", "Fuschia", None))
        self.label_2.setText(_translate("GroupBox", "Couleurs Mal placés :", None))
        self.couleur4.setItemText(0, _translate("GroupBox", "Rouge", None))
        self.couleur4.setItemText(1, _translate("GroupBox", "Jaune", None))
        self.couleur4.setItemText(2, _translate("GroupBox", "Vert", None))
        self.couleur4.setItemText(3, _translate("GroupBox", "Bleu", None))
        self.couleur4.setItemText(4, _translate("GroupBox", "Orange", None))
        self.couleur4.setItemText(5, _translate("GroupBox", "Blanc", None))
        self.couleur4.setItemText(6, _translate("GroupBox", "Violet", None))
        self.couleur4.setItemText(7, _translate("GroupBox", "Fuschia", None))
        self.couleur3.setItemText(0, _translate("GroupBox", "Rouge", None))
        self.couleur3.setItemText(1, _translate("GroupBox", "Jaune", None))
        self.couleur3.setItemText(2, _translate("GroupBox", "Vert", None))
        self.couleur3.setItemText(3, _translate("GroupBox", "Bleu", None))
        self.couleur3.setItemText(4, _translate("GroupBox", "Orange", None))
        self.couleur3.setItemText(5, _translate("GroupBox", "Blanc", None))
        self.couleur3.setItemText(6, _translate("GroupBox", "Violet", None))
        self.couleur3.setItemText(7, _translate("GroupBox", "Fuschia", None))
        self.nbBP.setText(_translate("GroupBox", "0", None))
        self.nbMP.setText(_translate("GroupBox", "0", None))

