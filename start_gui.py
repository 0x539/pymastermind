"""

this is the code to start the GUI of the game

Copyright (C) 2015 Corentin Bocquillon <corentin@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

# -*- coding: utf-8 -*-

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from gui import Ui_GroupBox
from mastermind import *

class Window(QGroupBox, Ui_GroupBox):
	def __init__(self, parent=None):
		super(Window, self).__init__(parent)
		self.setupUi(self)
		#qApp = QCoreApplication.instance()
		self.connect(self.btnEssayer, SIGNAL("clicked()"), self.tester)
		self.connect(self.btnNP, SIGNAL("clicked()"), self.nP)
		self.jeu = Mastermind()
		self.tester()

	def main(self):
		self.show()
		self.actualiserLbl()

	def tester(self):
		if self.jeu.gagne == False:
			"""il faut rajouter 1 car dans l'objet mastermind, 
			les couleurs commencent à 1 alors que l'index 
			commence à 0"""
			couleur1 = self.couleur1.currentIndex() + 1
			couleur2 = self.couleur2.currentIndex() + 1
			couleur3 = self.couleur3.currentIndex() + 1
			couleur4 = self.couleur4.currentIndex() + 1
			self.jeu.verifier(couleur1, couleur2, couleur3, couleur4)
			self.actualiserLbl()

	#nP = nouvelle partie
	def nP(self):
		#On supprime l'objet Mastermind
		del self.jeu
		#Puis on le recrée
		self.jeu = Mastermind()
		#On récupère nbMP et nbBP
		self.tester()
		#Et enfin, on actualise les labels
		self.actualiserLbl()

	def actualiserLbl(self):
		self.nbBP.setText(str(self.jeu.nbBP))

		if self.jeu.gagne == True:
			self.nbMP.setText("Gagné !")
		else:
			self.nbMP.setText(str(self.jeu.nbMP))

if __name__=='__main__':
	app = QApplication(sys.argv)
	window = Window()
	window.main()
	app.exec_()
